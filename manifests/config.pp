# == Class: packages
#
# Full description of class packages here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { packages:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2014 Your name here, unless otherwise noted.
#
class locales::config {

  File {
    owner => root,
    group => root,
    mode  => '0644'
  }

  file { '/etc/locale.gen.desired':
    content => template($locales::locale_gen_template),
  }

  exec { "/bin/cp -f ${locales::params::locale_gen_desired_file} ${locales::locale_gen_file}":
    subscribe   => File[$locales::locale_gen_desired_file],
    notify      => Exec[$locales::locale_gen_command],
    require     => File[$locales::locale_gen_desired_file],
    refreshonly => true
  }

  exec { $locales::locale_gen_command:
    refreshonly => true
  }

}
